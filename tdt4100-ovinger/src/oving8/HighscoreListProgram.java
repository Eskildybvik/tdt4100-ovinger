package oving8;

import java.util.Scanner;

import oving8.HighscoreListListener;

public class HighscoreListProgram implements HighscoreListListener {
	public HighscoreListProgram(HighscoreList list) {
		list.addHighscoreListListener(this);
	}

	@Override
	public void listChanged(HighscoreList list, int pos) {
		System.out.println("List changed at pos " + pos);
		System.out.println("List is now " + list.toString());
	}

	public static void main(String[] args) {
		HighscoreList list = new HighscoreList(3);
		HighscoreListProgram program = new HighscoreListProgram(list);
		
		Scanner cin = new Scanner(System.in);
		for (int i = 0; i < 6; ++i) {
			int input = cin.nextInt();
			list.addResult(input);
		}
	}

}
