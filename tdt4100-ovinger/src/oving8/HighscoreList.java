package oving8;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

public class HighscoreList {
	ArrayList<Integer> scores; 
	Collection<HighscoreListListener> listeners;
	int maxSize;

	public HighscoreList(int maxSize) {
		this.maxSize = maxSize;
		listeners = new ArrayList<HighscoreListListener>();
		scores = new ArrayList<Integer>();
	}
	
	public int size() {
		return scores.size();
	}
	
	public int getElement(int i) {
		return scores.get(i);
	}
	
	public void addResult(int score) {
		int pos = 0;
		for (int i = 0; i < scores.size(); ++i) {
			if (scores.get(i) <= score) pos++;
		}
		this.scores.add(pos, score);
		if (this.size() > maxSize) this.scores.remove(this.size()-1);
		final int param = (pos == this.size()) ? -1 : pos;
		listeners.stream().forEach(e -> {e.listChanged(this, param);});
	}
	
	public void addHighscoreListListener(HighscoreListListener listener) {
		if (!listeners.contains(listener)) {			
			listeners.add(listener);
		}
	}
	
	public void removeHighscoreListListener(HighscoreListListener listener) {
		listeners.remove(listener);
	}
	
	public String toString() {
		return scores.toString();
	}
}
