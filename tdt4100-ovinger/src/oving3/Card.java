package oving3;

public class Card {
	private char suit;
	private int face; 
	
	public Card(char suit, int face) throws IllegalArgumentException {
		if ("SHDC".indexOf(suit) == -1) throw new IllegalArgumentException("Suit must be S, H, D, or C");
		if (face < 1 || face > 13) throw new IllegalArgumentException("Face must be between 1 and 13");
		this.suit = suit;
		this.face = face;
	}
	
	public int getFace() {
		return face;
	}
	
	public char getSuit() {
		return suit;
	} 
	
	public String toString() {
		return "" + suit + face;
	}
}
