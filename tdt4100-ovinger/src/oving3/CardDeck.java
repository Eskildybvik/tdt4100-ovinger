package oving3;

import java.util.ArrayList;
import java.util.List;

public class CardDeck {
	private ArrayList<Card> cards = new ArrayList<Card>();
	
	public CardDeck(int n) throws IllegalArgumentException {
		if (n < 1 || n > 13) throw new IllegalArgumentException("n must be between 1 and 13");
		char[] suits = {'S', 'H', 'D', 'C'};
		for (char suit : suits) {
			for (int face = 1; face <= n; face++) {
				cards.add(new Card(suit, face));
			}
		}
	}
	
	public int getCardCount() {
		return cards.size();
	}
	
	public Card getCard(int n) {
		return cards.get(n);
	}
	
	public void shufflePerfectly() {
		int halfSize = cards.size()/2;
		List<Card> topHalf = cards.subList(0, halfSize);
		List<Card> bottomHalf = cards.subList(halfSize, cards.size());
		ArrayList<Card> tempCards = new ArrayList<Card>();
		
		for (int i = 0; i < halfSize; i++) {
			tempCards.add(topHalf.get(i));
			tempCards.add(bottomHalf.get(i));
		}
		
		cards.clear();
		cards = tempCards;
	}
}
