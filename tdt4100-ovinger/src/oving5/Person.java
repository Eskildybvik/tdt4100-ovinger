package oving5;

import java.util.ArrayList;

public class Person {
	private ArrayList<Person> children;
	private Person mother = null;
	private Person father = null;
	private String name;
	private char gender;

	public Person(String name, char gender) throws IllegalArgumentException {
		children = new ArrayList<Person>();

		this.name = name;
		if (gender != 'M' && gender != 'F')
			throw new IllegalArgumentException("Gender must be M or F");
		this.gender = gender;
	}

	public Person getMother() {
		return mother;
	}

	public Person getFather() {
		return father;
	}

	public String getName() {
		return name;
	}

	public char getGender() {
		return gender;
	}

	public int getChildCount() {
		return children.size();
	}

	public Person getChild(int n) throws IndexOutOfBoundsException {
		if (n > getChildCount())
			throw new IndexOutOfBoundsException("Invalid child index");
		return children.get(n);
	}

	public void setMother(Person mother) throws IllegalArgumentException {
		if (this.getMother() == mother) return;
		if (this == mother)
			throw new IllegalArgumentException("Can't set itself as mother.");
		if (this.getMother() != null) {
			this.getMother().removeChild(this);
			this.mother = null;
		}
		if (mother != null && mother.getGender() != 'F')
			throw new IllegalArgumentException("Mother must be female");
		this.mother = mother;
		if (mother != null)
			mother.addChild(this);
	}

	public void setFather(Person father) throws IllegalArgumentException {
		if (this.getFather() == father) return;
		if (this == father)
			throw new IllegalArgumentException("Can't set itself as father.");
		if (this.getFather() != null) {
			this.getFather().removeChild(this);
			this.father = null;
		}
		if (father != null && father.getGender() != 'M')
			throw new IllegalArgumentException("Father must be male");
		this.father = father;
		if (father != null)
			father.addChild(this);
	}

	public void addChild(Person child) throws IllegalArgumentException {
		if (this.children.contains(child)) return;
		if (this == child)
			throw new IllegalArgumentException("Can't have itself as a child.");
		this.children.add(child);
		if (this.getGender() == 'M') 
			child.setFather(this);
		else if (this.getGender() == 'F')
			child.setMother(this);
	}
	
	public void removeChild(Person child) {
		if (this.children.remove(child)) {
			if (this.getGender() == 'M')
				child.setFather(null);
			if (this.getGender() == 'F')
				child.setMother(null);			
		}
	}
}







