package oving9;

public class TrainCar {
	int deadWeight;
	
	public TrainCar(int deadWeight) {
		this.deadWeight = deadWeight;
	}

	public int getTotalWeight() {
		return deadWeight;
	}
	
	public void setDeadWeight(int newWeight) {
		deadWeight = newWeight;
	}
	
	public int getDeadWeight() {
		return deadWeight;
	}
}
