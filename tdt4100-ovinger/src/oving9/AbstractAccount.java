package oving9;

public abstract class AbstractAccount {
	protected double balance;

	public AbstractAccount() {
		this.balance = 0;
	}
	
	public void deposit(double amount) {
		if (amount < 0) throw new IllegalArgumentException("Illegal deposit amount");
		balance += amount;
	}
	
	public void withdraw(double amount) {
		if (amount < 0) throw new IllegalArgumentException("Illegal withdrawal amount");
		internalWithdraw(amount);
	}
	
	protected abstract void internalWithdraw(double amount) throws IllegalStateException;
	
	public double getBalance() {
		return balance;
	}
}
