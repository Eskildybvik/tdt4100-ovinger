package oving9;

public class DebitAccount extends AbstractAccount {

	public DebitAccount() {
		super();
	}

	@Override
	protected void internalWithdraw(double amount) throws IllegalStateException {
		if (this.getBalance() - amount < 0) throw new IllegalStateException();
		this.balance -= amount;
	}

}
