package oving9;

public class CreditAccount extends AbstractAccount {
	double creditLine;
	
	public CreditAccount(double creditLine) {
		super();
		this.setCreditLine(creditLine);
	}

	@Override
	protected void internalWithdraw(double amount) throws IllegalStateException {
		if (balance - amount < -creditLine) throw new IllegalStateException();
		balance -= amount;
	}

	public double getCreditLine() {
		return creditLine;
	}

	public void setCreditLine(double creditLine) {
		if (creditLine < 0) throw new IllegalArgumentException("Invalid credit line");
		if (balance < -creditLine) throw new IllegalStateException();
		this.creditLine = creditLine;
	}
	
	

}
