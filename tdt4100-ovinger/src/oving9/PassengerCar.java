package oving9;

public class PassengerCar extends TrainCar {
	final int personWeight = 80;
	int passengerCount;
	
	
	public PassengerCar(int deadWeight, int passengerCount) {
		super(deadWeight);
		this.passengerCount = passengerCount;
	}


	public int getPassengerCount() {
		return passengerCount;
	}


	public void setPassengerCount(int passengerCount) {
		this.passengerCount = passengerCount;
	}
	
	@Override
	public int getTotalWeight() {
		return this.getPassengerCount() * personWeight + this.getDeadWeight(); 
	}


	@Override
	public String toString() {
		return "PassengerCar [totalWeight=" + this.getTotalWeight() + ", passengerCount=" + this.getPassengerCount() + "]";
	}
}
