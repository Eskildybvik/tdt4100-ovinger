package oving9;

import java.util.ArrayList;

public class Train {
	ArrayList<TrainCar> cars;
	
	public Train() {
		cars = new ArrayList<TrainCar>();
	}

	public void addTrainCar(TrainCar car) {
		cars.add(car);
	}
	
	public boolean contains(TrainCar car) {
		return cars.contains(car);
	}
	
	public int getTotalWeight() {
		return cars.stream().map(e -> e.getTotalWeight()).reduce(0, (a, e) -> a + e);
	}
	
	public int getPassengerCount() {
		return cars.stream()
				.filter(e -> e.getClass() == PassengerCar.class)
				.map(e -> ((PassengerCar) e).getPassengerCount())
				.reduce(0, (a, e) -> a+e);
	}
	
	public int getCargoWeight() {
		return cars.stream()
				.filter(e -> e.getClass() == CargoCar.class)
				.map(e -> ((CargoCar) e).getCargoWeight())
				.reduce(0, (a, e) -> a+e);
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		cars.stream()
				.map(e -> e.toString())
				.forEachOrdered(s -> str.append(s));
		return str.toString();
	}
	
	
}
