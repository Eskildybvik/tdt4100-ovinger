package oving9;

public class SavingsAccount2 extends AbstractAccount {
	int withdrawals;
	final double fee;


	public SavingsAccount2(int withdrawals, double fee) {
		super();
		this.withdrawals = withdrawals;
		this.fee = fee;
	}


	@Override
	protected void internalWithdraw(double amount) throws IllegalStateException {
		if (balance - amount < 0 || (withdrawals <= 0 && balance - amount - fee < 0))
			throw new IllegalStateException();
		balance -= amount; 
		if (withdrawals <= 0) balance -= fee;
		--withdrawals;
	}

}
