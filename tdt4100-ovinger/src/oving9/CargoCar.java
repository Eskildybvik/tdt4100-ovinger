package oving9;

public class CargoCar extends oving9.TrainCar {
	int cargoWeight;
	
	public CargoCar(int deadWeight, int cargoWeight) {
		super(deadWeight);
		this.cargoWeight = cargoWeight;
	}

	@Override
	public int getTotalWeight() {
		return this.getDeadWeight() + this.getCargoWeight();
	}
	
	public int getCargoWeight() {
		return cargoWeight;
	}

	public void setCargoWeight(int cargoWeight) {
		this.cargoWeight = cargoWeight;
	}

	@Override
	public String toString() {
		return "CargoCar [cargoWeight=" + this.getCargoWeight() + ", totalWeight=" + this.getTotalWeight() + "]";
	}
}
