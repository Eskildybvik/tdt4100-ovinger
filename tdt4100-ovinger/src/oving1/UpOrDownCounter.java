package oving1;

public class UpOrDownCounter {
	int counter;
	int end; 
	
	public UpOrDownCounter(int start, int end) {
		if (start == end) throw new IllegalArgumentException("start and end can't be the same value");
		this.counter = start;
		this.end = end;
	}
	
	public int getCounter() {
		return counter;
	}
	
	boolean count() {
		if (end == counter) return false;
		counter += (counter < end) ? 1 : -1;
		return !(end == counter);
	}

	public static void main(String[] args) {
		UpOrDownCounter test1 = new UpOrDownCounter(0, 4);
		test1.count();
		test1.count();
		System.out.println(test1.getCounter());

		UpOrDownCounter test2 = new UpOrDownCounter(2, -4);
		test2.count();
		test2.count();
		test2.count();
		System.out.println(test2.getCounter());
		test2.count();
		test2.count();
		test2.count();
		System.out.println(test2.getCounter());
		test2.count();
		test2.count();
		System.out.println(test2.getCounter());
	}

}
