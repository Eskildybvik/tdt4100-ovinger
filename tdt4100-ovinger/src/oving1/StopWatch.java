package oving1;

public class StopWatch {
	int ticks = 0; 
	int time = -1;
	boolean started = false;
	boolean stopped = false;
	int lastLap = -1;
	int allPreviousLaps = 0; // Used for calculating last lap time

	public StopWatch() {
		// Doesn't do anything
	}
	
	public boolean isStarted() {
		return started;
	}
	
	public boolean isStopped() {
		return stopped;
	}
	
	public int getTicks() {
		return ticks;
	}

	public int getTime() {
		return time;
	}
	
	public int getLapTime() {
		// If lastLap is -1, it means this is the first lap, meaning the total time is also the lap time.
		return (lastLap == -1) ? this.getTime() : (this.getTime() - allPreviousLaps);
	}
	
	public int getLastLapTime() {
		return lastLap;
	}
	
	public void tick(int ticks) {
		this.ticks += ticks;
		// Only add ticks to time if the watch is both started and not stopped
		this.time += (started && !stopped) ? ticks : 0;
	}
	
	public void start() {
		if (!stopped) {
			started = true;
			time = 0;
		}
	}
	
	public void stop() {
		if (started) {
			stopped = true;
			lap();
		}
	}
	
	public void lap() {
		if (!started && !stopped) return; // Can't take a lap time if the watch hasn't started yet
		lastLap = getLapTime(); 
		allPreviousLaps += getLapTime();
	}
	
	public String toString() {
		return String.format("Ticks: %d, Time: %d, Started: %b, Stopped: %b, LastLap: %d, AllLastLaps: %d", ticks, time, started, stopped, lastLap, allPreviousLaps);
	}

	public static void main(String[] args) {
		StopWatch tester = new StopWatch();
		System.out.println(tester.toString());
		tester.tick(1000);
		System.out.println(tester.toString());
		tester.start();
		System.out.println(tester.toString());
		tester.tick(2300);
		System.out.println(tester.toString());
		tester.lap();
		System.out.println(tester.toString());
		tester.tick(3450);
		System.out.println(tester.toString());
		tester.lap();
		System.out.println(tester.toString());
		tester.tick(4567);
		System.out.println(tester.toString());
		tester.stop();
		System.out.println(tester.toString());
		tester.tick(1000000);
		System.out.println(tester.toString());
	}
}
