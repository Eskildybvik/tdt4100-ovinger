package oving1;

public class LineEditor {
	String text = ""; 
	int insertionIndex = 0;
	
	public String getText() {
		return text;
	}

	// Sets text and moves the cursor to the beginning
	public void setText(String text) {
		this.text = text;
		this.insertionIndex = 0;
	}

	public int getInsertionIndex() {
		return insertionIndex;
	}

	// Sets insertionIndex. If the provided index is less than 0 or greater than the length 
	// of the text, it's set to 0 ord text length.
	public void setInsertionIndex(int insertionIndex) {
		if (insertionIndex < 0) this.setInsertionIndex(0);
		this.insertionIndex = Math.min(insertionIndex, text.length());
	}
	
	// Moves insertionIndex n spaces left, or 0 if this would result in a negative index.
	public void left(int n) {
		insertionIndex = Math.max(0, insertionIndex-n);
	}
	
	// Same as left(1)
	public void left() {
		this.left(1);
	}
	
	// Moves insertionIndex n spaces right, or to the end of the text if this would result in a value greater than text length.
	public void right(int n) {
		insertionIndex = Math.min(text.length(), insertionIndex+n);
	}
	
	// Same as right(1)
	public void right() {
		this.right(1);
	}
	
	public void deleteRight() {
		if (insertionIndex == text.length()) return; // Don't delete if the cursor is at the end
		text = text.substring(0, insertionIndex) + text.substring(insertionIndex+1);
	}

	public void deleteLeft() {
		if (insertionIndex == 0) return; // Don't delete if the cursor is at the start
		text = text.substring(0, insertionIndex-1) + text.substring(insertionIndex);
		this.left();
	}
	
	public void insertString(String str) {
		text = text.substring(0, insertionIndex) + str + text.substring(insertionIndex);
		this.right(str.length());
	}
	
	// Calls the objects toString method, and inserts it
	public void insert(Object obj) {
		this.insertString(obj.toString());
	}
	
	public String toString() {
		return text.substring(0, insertionIndex) + "|" + text.substring(insertionIndex);
	}
	
	public LineEditor() {
		
	}

	public static void main(String[] args) {
		// I could not get the provided tests to work
		LineEditor tester = new LineEditor();
		tester.setText("Hallo");
		System.out.println(tester.toString());
		tester.right(2);
		System.out.println(tester.toString());
		tester.deleteLeft();
		System.out.println(tester.toString());
		tester.deleteRight();
		System.out.println(tester.toString());		
		tester.insertString("heihei");
		System.out.println(tester.toString());
		tester.left(3);
		System.out.println(tester.toString());
		tester.insert(tester);
		System.out.println(tester.toString());
		tester.setText("Blad");
		System.out.println(tester.toString());
		tester.setInsertionIndex(2);
		System.out.println(tester.toString());
	}
}
