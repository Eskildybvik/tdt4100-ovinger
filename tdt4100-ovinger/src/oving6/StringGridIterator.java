package oving6;

import java.util.Iterator;

public class StringGridIterator implements Iterator<String> {
	int currentRow = 0;
	int currentCol = 0;
	StringGrid grid;
	boolean rowMajor;
	
	public StringGridIterator(StringGrid grid, boolean rowMajor) {
		this.rowMajor = rowMajor;
		this.grid = grid;
	}
	
	@Override
	public boolean hasNext() {
		return !(currentRow == grid.getRowCount() || currentCol == grid.getColumnCount());
	}

	@Override
	public String next() {
		String element = grid.getElement(currentRow, currentCol);
		if (rowMajor) {
			currentCol++; 
			if (currentCol == grid.getColumnCount()) {
				currentCol = 0;
				currentRow++;
			}
		}
		else {
			currentRow++; 
			if (currentRow == grid.getRowCount()) {
				currentRow = 0;
				currentCol++;
			}
		}
		return element;
	}

	public void remove() {
		throw new UnsupportedOperationException();
	}
}
