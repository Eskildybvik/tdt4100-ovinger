package oving6;

import oving6.Named;

public class Person1 implements Named {
	private String givenName;
	private String familyName;
	
	public Person1(String givenName, String familyName) {
		this.givenName = givenName;
		this.familyName = familyName;
	}

	@Override
	public String getGivenName() {
		return givenName;
	}

	@Override
	public void setGivenName(String name) {
		this.givenName = name;
	}

	@Override
	public String getFamilyName() {
		return familyName;
	}

	@Override
	public void setFamilyName(String name) {
		this.familyName = name;
	}

	@Override
	public String getFullName() {
		StringBuffer fullName = new StringBuffer();
		fullName.append(givenName);
		fullName.append(' ');
		fullName.append(familyName);
		return fullName.toString();
	}

	@Override
	public void setFullName(String name) {
		String[] names = name.split(" ");
		this.givenName = names[0];
		this.familyName = names[1];
	}
	
	@Override
	public String toString() {
		return this.getFullName();
	}
}
