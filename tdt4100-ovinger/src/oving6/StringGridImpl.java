package oving6;

public class StringGridImpl implements StringGrid {
	private int numRows; 
	private int numCols;
	private String[][] grid; 
	
	public StringGridImpl(int rows, int cols) {
		this.numRows = rows;
		this.numCols = cols;
		grid = new String[numRows][numCols]; 
	}

	@Override
	public int getRowCount() {
		return numRows; 
	}

	@Override
	public int getColumnCount() {
		return numCols;
	}

	@Override
	public String getElement(int row, int column) {
		return grid[row][column];
	}

	@Override
	public void setElement(int row, int column, String element) {
		grid[row][column] = element;
	}
}
