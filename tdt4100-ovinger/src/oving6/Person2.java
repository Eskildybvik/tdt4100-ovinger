package oving6;

import oving6.Named;

public class Person2 implements Named {
	private String fullName;

	public Person2(String fullName) {
		this.fullName = fullName;
	}	

	@Override
	public String getGivenName() {
		return fullName.split(" ")[0];
	}

	@Override
	public void setGivenName(String name) {
		StringBuffer temp = new StringBuffer();
		temp.append(name);
		temp.append(" ");
		temp.append(fullName.split(" ")[1]);
		this.fullName = temp.toString();
	}

	@Override
	public String getFamilyName() {
		return fullName.split(" ")[1];
	}

	@Override
	public void setFamilyName(String name) {
		StringBuffer temp = new StringBuffer();
		temp.append(fullName.split(" ")[0]);
		temp.append(" ");
		temp.append(name);
		this.fullName = temp.toString();
	}

	@Override
	public String getFullName() {
		return fullName;
	}

	@Override
	public void setFullName(String name) {
		this.fullName = name;
	}
	
	@Override
	public String toString() {
		return this.getFullName();
	}
}
