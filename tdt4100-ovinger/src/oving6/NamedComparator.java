package oving6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class NamedComparator implements Comparator<Named> {
	public static void main(String[] args) {
		List<Named> names = new ArrayList<Named>(Arrays.asList(
			new Person1("Hans", "Pettersen"),
			new Person2("Bob K�re"),
			new Person2("BobFrank Olav"),
			new Person1("Tobias", "Kirkegaard"),
			new Person1("Frederique", "Paskjdfhjlasdhjflkashdfiuobhasiduf")			
		));
		Collections.sort(names, new NamedComparator());
		System.out.println(names.toString());
	}
	
	@Override
	public int compare(Named o1, Named o2) {
		int compFamilyName = o1.getFamilyName().compareTo(o2.getFamilyName());
		return (compFamilyName != 0) ? compFamilyName : o1.getGivenName().compareTo(o2.getGivenName());
	}
}
