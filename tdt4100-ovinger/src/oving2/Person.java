package oving2;

import java.util.Date;

public class Person {
	private final String ISO_CODES = "ad ae af ag ai al am ao aq ar as at au aw ax az "
			+ "ba bb bd be bf bg bh bi bj bl bm bn bo bq br bs bt bv bw by bz ca cc cd "
			+ "cf cg ch ci ck cl cm cn co cr cu cv cw cx cy cz de dj dk dm do dz ec ee "
			+ "eg eh er es et fi fj fk fm fo fr ga gb gd ge gf gg gh gi gl gm gn gp gq "
			+ "gr gs gt gu gw gy hk hm hn hr ht hu id ie il im in io iq ir is it je jm "
			+ "jo jp ke kg kh ki km kn kp kr kw ky kz la lb lc li lk lr ls lt lu lv ly "
			+ "ma mc md me mf mg mh mk ml mm mn mo mp mq mr ms mt mu mv mw mx my mz na "
			+ "nc ne nf ng ni nl no np nr nu nz om pa pe pf pg ph pk pl pm pn pr ps pt "
			+ "pw py qa re ro rs ru rw sa sb sc sd se sg sh si sj sk sl sm sn so sr ss "
			+ "st sv sx sy sz tc td tf tg th tj tk tl tm tn to tr tt tv tw tz ua ug um "
			+ "us uy uz va vc ve vg vi vn vu wf ws ye yt za zm zw";
	private String name; 
	private char gender = '\0';
	private String email = null;
	private Date birthday = null;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) throws IllegalArgumentException {
		if (!validateName(name)) throw new IllegalArgumentException("A name must contain only a first and a last name seperated by a space, both at least two letters");
		this.name = name;
	}
	
	public char getGender() {
		return gender;
	}
	
	public void setGender(char gender) throws IllegalArgumentException {
		if (!validateGender(gender)) throw new IllegalArgumentException("Invalid gender, must be M, F, or \0");
		this.gender = gender;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) throws IllegalArgumentException {
		if (!validateEmail(email, this.name)) throw new IllegalArgumentException("Invalid email");
		this.email = email;
	}
	
	public Date getBirthday() {
		return birthday;
	}
	
	public void setBirthday(Date birthday) {
		if (!validateBirthday(birthday)) throw new IllegalArgumentException("Birthday can't be in the future");
		this.birthday = birthday;
	}
	
	private boolean validateName(String name) {
		return name.matches("[A-Z���][a-z���]+\\s[A-Z���][a-z���]+");
	}
	
	private boolean validateGender(char gender) {
		return gender == 'M' || gender == 'F' || gender == '\0';
	}
	
	private boolean validateEmail(String email, String name) {
		// Check email formatting, and if the domain TLD is valid
		if (!(email.substring(0, email.length()-2).matches("[A-Za-z������]+\\.[A-Za-z������]+@[A-Za-z������0-9\\.\\-]+\\.") &&
				ISO_CODES.contains(email.substring(email.length()-2)))) return false;

		// Checking for email matching name
		return email.substring(0, email.indexOf("@")).replace(".", " ").contentEquals(name.toLowerCase());
	}
	
	private boolean validateBirthday(Date birthday) {		
		return birthday.before(new Date());
	}
	

}
