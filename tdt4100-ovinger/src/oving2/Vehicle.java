package oving2;

public class Vehicle {
	private char type; 
	private char fuel; 
	private String registrationNumber;
	
	public Vehicle(char type, char fuel, String registrationNumber) {
		if (!(type == 'M' || type == 'C')) throw new IllegalArgumentException("Invalid vehicle type");
		this.type = type;
		
		if (!(fuel == 'G' || fuel == 'D' || fuel == 'E' || (fuel == 'H' && type == 'C'))) 
			throw new IllegalArgumentException("Invalid fuel type");
		this.fuel = fuel;
		
		this.setRegistrationNumber(registrationNumber);
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		if (!validateRegistrationNumber(registrationNumber, this.fuel, this.type)) throw new IllegalArgumentException("Invalid registration number");
		this.registrationNumber = registrationNumber;
	}

	public char getVehicleType() {
		return type;
	}

	public char getFuelType() {
		return fuel;
	}
	
	private boolean validateRegistrationNumber(String regNum, char fuel, char type) {
		String regex = "";
		switch (fuel) {
		case 'E': regex += "(EL|EK)"; 
			break;
		case 'H': regex += "(HY)";
			break;
		default: regex += "(?!EL|EK|HY)([A-Z]{2})";
			break;
		}
		switch (type) {
		case 'C': regex += "[0-9]{5}";
			break;
		case 'M': regex += "[0-9]{4}";
			break;
		}
		return regNum.matches(regex);
	}
	
}
